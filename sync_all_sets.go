package sync

import (
	"context"
	"database/sql"
	"os"

	"cloud.google.com/go/pubsub"

	kitlog "github.com/go-kit/kit/log"

	_ "github.com/lib/pq"
)

type PubSubMessage struct {
	Data []byte `json:"data"`
}

var (
	psql   *sql.DB
	logger kitlog.Logger

	topic *pubsub.Topic
)

func init() {
	var err error

	{
		logger = kitlog.NewJSONLogger(os.Stdout)
		logger = kitlog.With(logger, "timestamp", kitlog.DefaultTimestampUTC, "transport", "pubsub")
	}

	{
		psql, err = sql.Open("postgres", os.Getenv("DATABASE_URL"))
		if err != nil {
			logger.Log("level", "fatal", "msg", "could not connect to database", "err", err)
			os.Exit(1)
		}
		logger.Log("level", "debug", "msg", "connected to database", "url", os.Getenv("DATABASE_URL"))
	}

	// Only allow 1 connection to the database to avoid overloading it.
	psql.SetMaxIdleConns(1)
	psql.SetMaxOpenConns(1)

	{
		client, err := pubsub.NewClient(context.Background(), "synkt-1556838469966")
		if err != nil {
			logger.Log("level", "fatal", "msg", "error creating pubsub client", "err", err)
			os.Exit(1)
		}

		topic = client.Topic("sync-set-prices")
	}
}

func SyncAllSets(ctx context.Context, m PubSubMessage) error {
	var (
		rows *sql.Rows
		err  error
	)
	logger.Log("level", "debug", "msg", "received message", "data", string(m.Data))

	rows, err = psql.Query("SELECT code FROM card_sets")
	if err != nil {
		logger.Log("level", "error", "msg", "error executing query", "err", err)
		return err
	}
	defer rows.Close()

	for err == nil && rows.Next() {
		var code string

		err = rows.Scan(&code)
		if err != nil {
			continue
		}

		logger.Log("level", "info", "msg", "publishing message for set", "code", code)
		_, err = topic.Publish(ctx, &pubsub.Message{
			Data: []byte(code),
		}).Get(ctx)
		if err != nil {
			continue
		}
	}

	logger.Log("level", "info", "msg", "completed function", "err", err)
	return err
}
