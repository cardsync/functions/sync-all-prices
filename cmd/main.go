package main

import (
	"context"

	sync "gitlab.com/cardsync/sync-all-sets"
)

func main() {
	err := sync.SyncAllSets(context.Background(), sync.PubSubMessage{Data: []byte("grn")})

	println(err)
}
