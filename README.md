# Functions - Sync All Sets

This is a Google Cloud Function that initiates a full price sync for all cards in Synkt. It's triggered by a Cloud Scheduler action in GCP.

It iterates each set and sends a PubSub message for that set, which the `sync-set-prices` function consumes.
